import { createContext } from 'react'

export const chatContext = createContext()
export const formContext = createContext()
export const videCallContext = createContext()