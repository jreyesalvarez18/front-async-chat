import { useRef } from 'react'
import { ChildrenProps } from '../../../interfaces/InterfaceChat'

export const ChatFormFile = ({ handleFile }: ChildrenProps): JSX.Element => {
    const file = useRef<HTMLInputElement>(null)

    const viewFiles = () => {
        if (file.current !== null && handleFile) {
            handleFile(file.current.files)
        }
    }
    
    return (
        <div className="container_form_file" onChange={viewFiles}>
            <label htmlFor="file-input" style={{cursor:'pointer'}}>
                <i  className="fa-solid fa-paperclip"></i>
            </label>
            {/* accept=".jpg,.png,.pdf"  */}
            <input id="file-input" multiple type="file"  ref={ file } style={{display: 'none'}}/>
        </div>
    )
}
