import React, { useState, useContext, useRef } from 'react'
import { chatContext, formContext } from '../../../tenants/chatContext'
import { usePost } from '../../../customHook/axios/Post'
import { ChatFormFile } from './ChatFormFile'
import { ChatUploadFile } from './ChatUploadFile'
import { Attachment, Message } from '../../../interfaces/InterfaceChat'

export const ChatForm = () => {
    const { URL, handleMessage, role } = useContext(chatContext)
    // const apiEjemplo = 'http://localhost:4500/api/user/add'
    // const URL1 = 'https://6982-190-162-94-147.ngrok.io/api/user/add'
    const { post, postFile } = usePost(URL) // colocar URL

    const initialMessage: Message = {
        message: '',
        attachments: [],
        user:'jean',
        role,
        // _id: new Date() // sacar este atributo
    }
    const [ message, setMessage ] = useState(initialMessage)
    const [ heightTextArea, setheightTextArea ] = useState(36)
    const refTextArea = useRef<HTMLTextAreaElement>(null)

    const handleInput = ({target}: React.ChangeEvent<HTMLTextAreaElement>) => {
        setheightTextArea(target.scrollHeight)
        setMessage({
            ...message,
            message: target.value
        })
    }

    const handleFile = (file: FileList):void => { // se ejecuta cada vez que se agrega un file
        const { attachments } = message
        if (attachments) {
            setMessage({
                ...message,
                attachments: [...attachments, ...file] as File []
            })
        }
    }

    const handleSubmit = (e?: React.FormEvent<HTMLFormElement> ) => { 
        if (e) {
            e.preventDefault()
        }
        
        if (message.message.length > 0 && (message.attachments !== undefined && message.attachments.length > 0)) {
               console.log("mensaje con archivo");
            postFile(message)
            .then((data)=>{
                handleMessage(data)
                setMessage(initialMessage)
            })
        
        } else if (message.message.length > 0) {
            console.log("solo mensaje")
            post(message)
            .then((data)=>{
                handleMessage(data)
                setMessage(initialMessage)
            })
        } else {
            console.log("solo file")
            postFile(message)
            .then((data)=>{
                handleMessage(data)
                setMessage(initialMessage)
            })
        }
    }

    return (

        // ordenar el formulario File con composicion de componentes
        <formContext.Provider value={{   
            message,
            setMessage
        }}>
            <form onSubmit={handleSubmit}>
                <div className="container_form">
                    <ChatFormFile handleFile={handleFile} />
                    <div className="input-group">
                        <textarea 
                            ref= { refTextArea }
                            className="form-control"
                            value={message.message}
                            placeholder="Escribe un mensaje..."
                            onChange={ handleInput }
                            //onKeyPress= { enterSubmit }
                            style={{height: heightTextArea, maxHeight:'132px'}}
                        />
                        <button className="btn btn-outline-secondary"><i className="fa-solid fa-paper-plane"></i></button>
                    </div>
                </div>
                {
                    <ChatUploadFile />
                }    
            </form>
        </formContext.Provider>
    )
}
