import { useParams } from 'react-router-dom'
import { ChatAsync } from './ChatAsync'
import { ChatForm } from './ChatForm'
import { ChatMessages } from './ChatMessages'
import { ChatVideoCall } from '../phone/ChatVideoCall'
import { ButtonICon } from '../phone/ButtonICon'
// import './style.css'

export const Chat = () => {
	const { call } = useParams()
	
	return (
		// logica de chat Asyncrono
		<ChatAsync>
			<div className="card">
				<div className="container_header">
					<div className="container_title_video_call">
						{
							call !== 'call' ?
								<p style={{ textAlign:'left' }}> Chat Asíncrono </p>	
							: <>		
								<p style={{ textAlign:'left' }}> Chat Asíncrono </p>
								<div>
									<ChatVideoCall>
										<ButtonICon/>
									</ChatVideoCall>		
								</div>
							</>
						}
					</div>
				</div>
				<div>
					<ChatMessages/>
					<ChatForm/>
				</div>
			</div>
		</ChatAsync>
	)
}
