import { useState, useEffect } from 'react'
import {  useParams } from 'react-router-dom'
import { useGet } from '../../../customHook/axios/Get'
import { chatContext } from '../../../tenants/chatContext'

import { ChildrenProps, Message } from '../../../interfaces/InterfaceChat'
export const ChatAsync = ({ children }: ChildrenProps): (JSX.Element | null)  => {
    // https://4125-190-114-32-200.ngrok.io/v1/facl/chats
    const URL = 'https://c789-190-114-32-200.ngrok.io/v1/facl/chats/62281c2d7f5c048e88b7c370/messages'
    // const apiEjemplo = 'https://6982-190-162-94-147.ngrok.io/api/user/get'
    // const URL = 'https://6982-190-162-94-147.ngrok.io/api/user/get'
    const [ msg, setMsg ] = useState<Message>()
    const { role } = useParams() // cambiar a Chat
    const { data, loading, error , getAll } = useGet(URL)


    // useEffect(()=>{
    //     setInterval(()=>{
    //         const reload = false 
    //         getAll(reload)
    //     },10000)
    // },[])

    const handleMessage = (message: Message) => {
        
        const reload = false // para que no haga el loading
        if (message) {
            getAll(reload)
            .then(()=>{
                setMsg(message) 
            })
        }
    }

    if (loading) {
        return  <p>...Loading</p>
    }

    if (error) {
        <p>{ error }</p>
    }

    if (!data) {
        return null
    }

    return (
        <chatContext.Provider value={{
            data,
            URL,
            msg,
            role,
            handleMessage
        }}>
            {children}
        </chatContext.Provider>
    )
}
