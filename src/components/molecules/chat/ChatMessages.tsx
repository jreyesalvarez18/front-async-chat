import { useContext, useEffect, useRef } from 'react'
import { chatContext } from '../../../tenants/chatContext'
import { Message } from './Message'
import { Message as MessageInterface } from '../../../interfaces/InterfaceChat'


export const ChatMessages = (): JSX.Element => {

  const scrollContainer = useRef<HTMLDivElement>(null)
  const { data, msg } = useContext(chatContext)

  useEffect(()=>{   
      if ( scrollContainer.current !== null ){
        scrollContainer.current.scrollTop = scrollContainer.current.scrollHeight;
      }
  },[data, msg])

  return (
    <div className="container_message mb-4" ref={ scrollContainer }>
        {
          data.map((msg: MessageInterface ) => (<Message key={String(msg._id)} msg={msg}/>))
          // data.map(msg => (<Message key={msg._id} msg={msg}/>))
        }
    </div>
  )
}
