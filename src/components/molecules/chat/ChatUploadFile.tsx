import { useContext } from 'react'
import { formContext } from '../../../tenants/chatContext'

export const ChatUploadFile = (): JSX.Element => {
  const { message, setMessage } = useContext(formContext)

  const removeFile = (index: number) => {
    let array = message.attachments
    array.splice(index, 1)
    setMessage({
      ...message,
      attachments: array
    })
  }

  return (
    <> 
      {
        message.attachments.map((file:File, index: number)=> (
          <p key={ index }>
            { file.name }
            <span onClick={() => removeFile(index)}> :X </span>
          </p>
        ))
      }
    </>
  )
}
