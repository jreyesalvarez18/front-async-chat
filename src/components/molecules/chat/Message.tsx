import { useContext } from 'react';
import { saveAs } from 'file-saver';
import { PropFiles, Attachment, ChildrenProps } from '../../../interfaces/InterfaceChat';
import { chatContext } from '../../../tenants/chatContext'


export const Message = ({msg}: ChildrenProps ): JSX.Element => {
  const { role: roleContext } = useContext(chatContext)
  const { message, timestamp, attachments, role } = msg

  const ComponentFile = ({ files }: PropFiles) => {

    const download = (file: Attachment) => {
      const { contentType, content, name } = file
      const base64 = `data:${contentType};base64,${content}`
      fetch(base64).then(async data => saveAs(await data.blob(), name ))
    }

    const nameFile = (name:string): string => {
      if (name.length > 20) {
        return name.slice(0, name.length - (name.length - 20))+'...'
      } else {
        return name
      }
    }

      return <>
        {
          files.map((file: Attachment, index: number) => (
            <div 
              className="container_file"
              key={index} 
              >
                <div className="container_download">
                    <i className="fa-solid fa-file"></i>
                    <span>{nameFile(file.name)}</span>
                    <i className="fa-solid fa-download" style={{float: 'right', cursor:'pointer'}} onClick={ ()=> download(file) }></i>
                </div>
            </div>
          ))
        }
      </>
  }

  return ((roleContext === role) ? <>
          <div className="message_date">{timestamp}</div>
          <div className={'message_user'}>
              <div className={'message_user_text'}>
                  { message }
                {(attachments && attachments.length > 0) && <ComponentFile files={attachments}/>}
              </div>
          </div> 
        </> 
        : <>
          <div className="message_date">{timestamp}</div>
          <div className={'message_agente'}>
            <div className={'message_agente_text'}>
                { message }
              {(attachments && attachments.length > 0) && <ComponentFile files={attachments}/>}
            </div>
          </div>
        </>
  )
}
