import { useContext } from 'react'
import { videCallContext } from '../../../tenants/chatContext'
import { ChildrenProps } from '../../../interfaces/InterfaceChat'

export const ButtonICon = (): JSX.Element  => {
    const { videoCall, setVideoCall } = useContext(videCallContext)


    const init = (type: string) => {
        setVideoCall({
            ...videoCall,
            [type]: true
        })     
    }

    return (
            <>
                <button style={{marginRight:'5px'}} onClick={() => init('audio')}> <i className="fa-solid fa-phone"></i>  </button>
                <button style={{marginRight:'5px'}} onClick={() => init('video')}> <i className="fa-solid fa-video"></i> </button>
            </>
    )
}
