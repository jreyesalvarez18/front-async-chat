import { useState, useEffect, useRef } from 'react'
import { ChildrenProps } from '../../../interfaces/InterfaceChat'
import { videCallContext } from '../../../tenants/chatContext'
import { Video } from './Video'
import { VideoCall as ClassVideoCall } from '../../../clases/VideoCall'


export const ChatVideoCall = ({ children }: ChildrenProps): (JSX.Element | null)  => {
  const [ videoCall, setVideoCall ] = useState({
    video: false,
    audio: false
  })
  const lbNumber = useRef(null)

  useEffect(()=> {
    // init todo
    if (videoCall.audio || videoCall.video) {
      new ClassVideoCall(lbNumber, videoCall.video, videoCall.audio).init()
    }
    
  },[videoCall])

  return (
    <videCallContext.Provider value={{
      videoCall,
      setVideoCall,
      lbNumber
    }}>
      <>
        <div className="container_video_call">
          {children}
        </div>
        {/* aqui va modal o nueva ventana  */}
        {/* {JSON.stringify(videoCall)} */}
        <div >
            <Video/>
        </div>
      </>
    </videCallContext.Provider>
  )
}
