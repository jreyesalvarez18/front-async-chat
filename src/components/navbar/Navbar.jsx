import { NavLink } from "react-router-dom";

export const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-sm navbar-light bg-light">
        <div className="container">     
            <div className="row collapse navbar-collapse" id="navbarNav">
                <ul className="col navbar-nav">
                    <NavLink 
                        className={({isActive})=> 'nav-item nav-link ' + (isActive ? 'active' : '')}
                        to="/chat"
                    >
                        Chat 
                    </NavLink>
                    <NavLink 
                        className={({isActive})=> 'nav-item nav-link ' + (isActive ? 'active' : '')}
                        to="/compras"
                    >
                        Mis Compras
                    </NavLink>
                </ul>
            </div>
        </div>
    </nav>
  )
}
