import { Routes,Route } from 'react-router-dom'
import { Compras } from '../molecules/compras/Compras'
import { Chat } from '../molecules/chat/Chat'
// import { Navbar } from '../navbar/Navbar'


export const Dashboard = () => {
  return (
    <>
        {/* <Navbar/> */}
        <div className="container mt-4">
            <Routes>
                <Route path="/chat/:role" element={<Chat/>}/>
                <Route path="/chat/:role/:call" element={<Chat/>}/>
                <Route path="/compras"  element={<Compras/>}/>
                <Route path="/" element={<Chat />} />  
            </Routes>
        </div>
    </>
  )
}
