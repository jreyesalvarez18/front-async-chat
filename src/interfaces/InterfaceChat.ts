import { ReactChild, ReactFragment, ReactPortal } from 'react'

export interface ChildrenProps {
    children?: ReactChild | ReactFragment | ReactPortal | string | boolean | null | undefined
    handleFile?: Function
    cleanFile?: boolean
    msg?: Message | any
    // render?: ReactChild | ReactFragment | ReactPortal | string | boolean | null | Function | JSX.Element
}
export interface Message {
    _id?: string | Date
    message: string
    timestamp? :string 
    role: string
    user: string
    chatId?: string
    attachments?: FileList | File [] | Attachment []
}

export interface GetData { 
    data: Message[] 
    loading: boolean
    error: string | undefined
    getAll: Function 
}

export interface ResponseGet {
    data?: Message[]
}

export interface ContextChat {
    data: Message []
    URL: string
    msg: Message | undefined
    role: string | undefined
    handleMessage: Function
}

//FILES

export interface Attachment {
    name: string
    contentType: string
    content: string | unknown | Blob
}


export interface PropFiles {
    files: Attachment []
}

export interface PropFileList {
    fileList?: FileList,
    setArrayFiles: (array: File[]) => void
}
