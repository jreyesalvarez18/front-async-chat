import { useState } from 'react'
import axios from 'axios'
import { Message, Attachment } from '../../interfaces/InterfaceChat'
// import saveAs from 'file-reader'

export const usePost = (url: string) => {
    const [ data, seData ] = useState(null)
    const [ loading, setLoading ] = useState(false)
    const [ error, setError ] = useState(null)
    
    const post = async (body: Message) => {
        try {
            setLoading(true)
            const response = await axios.post(url, body)
            seData(response.data)
            return response.data

        } catch (e: any) {
            setError(e)
        } finally {
            setLoading(false)
        }
    }

    const postFileJSON = async (body: Message) => {
        console.log("body", body);
        let objectFile: Attachment;
        let arrayFile: Attachment[] = []
        let file: any // esta wea
        const { attachments } = body

        if (attachments !== undefined){ 
            for(file of attachments) {
                objectFile = {
                    name: file.name,
                    contentType: file.type,
                    content: await toBase64(file)
                }
                arrayFile = [...arrayFile, objectFile]
            }
        }


        console.log(arrayFile[0].contentType);

        try {
            const response = await axios.post(url, {
                ...body,
                attachments: arrayFile
            })
            seData(response.data)
            console.log(response.data);
            return response.data

        } catch (e:any) {
            setError(e)
        } finally {
            setLoading(false)
        }
    }

    const postFile = async (body: Message) => {
        console.log("body", body);
        let objectFile: Attachment;
        let arrayFile: Attachment[] = []
        let file: any // esta wea
        const { attachments } = body

        if (attachments !== undefined){ 
            for(file of (attachments as File [])) {
                objectFile = {
                    name: file.name,
                    contentType: file.type,
                    content: await toBase64(file)
                }
                arrayFile = [...arrayFile, objectFile]
            }
        }


        console.log(arrayFile[0].contentType);
        body = {
            ...body,
            attachments: arrayFile
        }

        try {
            const response = await axios.post(url, body)
            seData(response.data)
            console.log(response.data);
            return response.data

        } catch (e:any) {
            setError(e)
        } finally {
            setLoading(false)
        }
    }

    const toBase64 = (file: Blob) => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
            let encoded = reader.result !== null ? reader.result.toString().replace(/^data:(.*,)?/, '') : null
            if (encoded && (encoded.length % 4) > 0) {
              encoded += '='.repeat(4 - (encoded.length % 4));
            }
            resolve(encoded);
        }
        reader.onerror = (error) => {
            reject(error);
        }
    });

    return {
       post,
       postFile,
       data,
       loading,
       error
    }
}